/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
#elif _POSIX_C_SOURCE < 200809L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <sys/wait.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

static int aflag = 0;		/* whether to feed input as an argument */
static int cflag = 0;		/* whether to continue if the utility failed */
static int iflag = 0;		/* whether to read input from a file */
static int vflag = 0;		/* whether to print verbose output */

static char delimiter = '\n';	/* line delimiter */
static FILE *ifile = 0;		/* file from which input is read */
static FILE *tfile = 0;

static void
usage(void)
{
	(void)fprintf(stderr, "usage: consume [-0ach] [-i file] [-v]"
	    " [utility [argument ...]]\n");
	exit(1);
}

static char *
readinput(char * restrict str, size_t size)
{
	size_t i;
	int ch;

	if (ifile == 0) {
		return (0);
	}
	if (size == 0) {
		return (str);
	}

	for (i = 0; i < size-1; i++) {
		ch = fgetc(ifile);
		if (ch == EOF) {
			if (feof(ifile)) {
				str[i] = 0;
			}
			return (0);
		} else if (ch == delimiter) {
			str[i] = (char)ch;
			str[i+1] = 0;
			return (str);
		} else {
			str[i] = (char)ch;
		}
	}
	str[size-1] = 0;

	return (str);
}

static int
execute(char *cmd[], int nargs, char *data)
{
	int ipc[2];
	char **args;
	size_t count;
	pid_t pid;
	int arg;
	int status;
	int ret;

	ret = 0;
	args = calloc((size_t)nargs+2, sizeof(*args));
	if (args == 0) {
		ret = 1;
		perror("consume");
		goto alloc_err;
	}

	for (arg = 0; arg < nargs; arg++) {
		args[arg] = cmd[arg];
	}

	if (aflag == 1) {
		args[arg] = data;
		args[arg+1] = 0;
	}

	if (vflag == 1) {
		(void)fprintf(stderr, "Executing `%s ", cmd[0]);
		for (arg = 1; arg < nargs; arg++) {
			(void)fprintf(stderr, "%s ", cmd[arg]);
		}
		(void)fprintf(stderr, "%s`\n", data);
	}

	if (aflag == 0) {
		if (pipe(ipc) < 0) {
			ret = 1;
			perror("consume");
			goto pipe_err;
		}
	}

	pid = fork();
	if (pid < 0) {
		ret = 1;
		perror("fork failed");
		goto fork_err;
	} else if (pid == 0) {
		if (aflag == 0) {
			(void)close(ipc[1]);
			if (ipc[0] != STDIN_FILENO) {
				if (dup2(ipc[0], STDIN_FILENO) !=
				    STDIN_FILENO) {
					ret = 1;
					perror("consume");
					goto dup_err;
				}
			}
		}

		if (execvp(*args, args) < 0) {
			ret = 1;
			perror("consume");
			goto exec_err;
		}
	} else {
		if (aflag == 0) {
			(void)close(ipc[0]);
			count = strlen(data);
			if (write(ipc[1], data, count) != (ssize_t)count) {
				ret = 1;
				perror("consume");
				goto write_err;
			}
			(void)close(ipc[1]);
		}
	}

	if (waitpid(pid, &status, 0) < 0) {
		ret = 1;
		perror("consume");
		goto wait_err;
	}

	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status) != 0) {
			if (iflag == 1) {
				(void)fprintf(tfile, "%s%c", data,
				    delimiter);
			}
			if (cflag == 0) {
				ret = 1;
			}
		}
	} else if (WIFSIGNALED(status)) {
		if (WTERMSIG(status) < SIGRTMAX) {
			(void)fprintf(stderr, "%s terminated by "
			    "SIG%s\n", args[0],
			    strsignal(WTERMSIG(status)));
		} else {
			(void)fprintf(stderr, "%s terminated by "
			    "signal %d\n", args[0],
			    WTERMSIG(status));
		}

		if (cflag == 0) {
			ret = 1;
		}
	}

wait_err:
write_err:
exec_err:
dup_err:
fork_err:
pipe_err:
	free(args);
alloc_err:
	return (ret);
}

static int
consume(char *cmd[], int nargs)
{
	char buf[LINE_MAX];
	int ret;

	ret = 0;
	while (readinput(buf, LINE_MAX) != 0) {
		buf[strcspn(buf, "\n")] = 0;
		if (ret == 0) {
			ret = execute(cmd, nargs, buf);
		} else {
			if (iflag == 0) {
				break;
			}

			(void)fprintf(tfile, "%s%c", buf, delimiter);
		}
	}

	return (ret);
}

static int
consume_file(char *src, char *cmd[], int nargs)
{
	char template[] = "tmp.XXXXXX";
	int fd;
	int ret;

	ifile = fopen(src, "r");
	if (ifile == 0) {
		ret = 1;
		perror("consume");
		goto infile_open_err;
	}

	fd = mkstemp(template);
	if (fd < 0) {
		ret = 1;
		perror("consume");
		goto mkstemp_err;
	}

	tfile = fdopen(fd, "w");
	if (tfile == 0) {
		ret = 1;
		perror("consume");
		goto tmpfile_open_err;
	}

	ret = consume(cmd, nargs);

	assert(tfile != 0);
	(void)fclose(tfile);
	(void)fclose(ifile);
	ifile = 0;

	if (remove(src) != 0) {
		ret = 1;
		perror("consume");
		goto remove_err;
	}

	if (rename(template, src) != 0) {
		ret = 1;
		perror("consume");
		goto rename_err;
	}

rename_err:
remove_err:
tmpfile_open_err:
	if (ifile != 0) {
		(void)fclose(ifile);
	}
	(void)unlink(template);
mkstemp_err:
infile_open_err:
	return (ret);
}

int
main(int argc, char *argv[])
{
	char src[BUFSIZ] = {0};
	int opt;
	int ret;

	while ((opt = getopt(argc, argv, "+0achi:v")) != -1) {
		switch (opt) {
		case '0':
			delimiter = '\0';
			break;
		case 'a':
			aflag = 1;
			break;
		case 'c':
			cflag = 1;
			break;
		case 'h':
			usage();
			break;
		case 'i':
			iflag++;
			if (iflag > 1) {
				(void)fprintf(stderr, "consume: cannot have"
				    " multiple input files.\n");
				exit(1);
			}

			(void)memcpy(src, optarg, strlen(optarg));
			break;
		case 'v':
			vflag = 1;
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		(void)fprintf(stderr, "consume: you need to specify an utility"
		    " to invoke\n");
		exit(1);
	}

	if (iflag == 0) {
		ifile = stdin;
		ret = consume(argv, argc);
	} else {
		ret = consume_file(src, argv, argc);
	}

	return (ret);
}
